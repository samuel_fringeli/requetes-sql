var mysql = require('mysql');

var pool = mysql.createPool({
  host     : 'localhost',
  user     : 'root',
  password : 'root',
  database : 'horaire_ecole',
  charset  : 'UTF8_UNICODE_CI',
  multipleStatements: true
});

query_error = function(erreur) {
  if (erreur) {
    console.log('query error : ' + erreur.stack);
  }
}


exports.requete = function(requete_sql, callback) {
  pool.getConnection(function(err, connection) {
    connection.query(requete_sql, function(err, results) {
      query_error(err);
      /* nous testons l'existence du paramètre callback, qui est facultatif si nous
      ne voulons pas obtenir le résultat de la requête (par exemple pour les UPDATE) */
      if(typeof(callback) !== 'undefined') {
        callback(results);
      }
      connection.destroy();
    });
  });
}