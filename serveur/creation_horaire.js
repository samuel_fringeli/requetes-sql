/* 
import :
	data.duree
	data.jour
	data.h_debut
	data.mat_code
	data.classe
	data.prof_nom
	data.prof_prenom
	data.salle

export :
	id_periode
	id_branche
	id_classe
	id_prof
	id_salle
	duree
*/

var sql = require('./sql.js');

function existence_test(data) {
	if (data === '' || typeof(data) === 'undefined') {
		return false;
	}
	return true;
}

function test(to_test) {
	var all_ok = true;
	if (Array.isArray(to_test)) {
		for (var i = 0; i < to_test.length; i++) {
			if (!existence_test(to_test[i])) {
				all_ok = false;
			}
		}
	}
	else {
		if (!existence_test(to_test)) {
			all_ok = false;
		}
	}
	return all_ok;
}

function add_test(to_test, to_add) {
	if (test(to_test)) {
		return to_add;
	} else {
		return '';
	}
}

function requete_test(to_test, requete, callback) {
	if (test(to_test)) {
		sql.requete(requete, function(results) {
			callback(results);
		});
	} else {
		callback([]);
	}
}

function tests_generaux(i, to_test, requetes, callback) {
	if (i < 0) {
		callback(to_test);
		return;
	}
	requete_test(to_test[i], requetes[i], function(results){
		if (!existence_test(results[0])) { 
			to_test[i] = ''; 
		}
		tests_generaux(i-1, to_test, requetes, callback);
	});
}

function verification_donnees(data, callback) {
	var to_test = [data.jour, data.h_debut, data.mat_code, data.classe, data.prof_nom, data.prof_prenom, data.salle];
	data.salle = data.salle.replace(/([0-9]+)\ \([/A-Za-z]+\)/, '$1');
	var requetes = [
		'SELECT id_jour FROM jours WHERE jour = "'+data.jour+'"',
		'SELECT id_heure FROM heures WHERE heure_debut = "'+data.h_debut+'"',
		'SELECT id_branche FROM branches WHERE code_branche = "'+data.mat_code+'"',
		'SELECT id_classe FROM classes WHERE classe = "'+data.classe+'"',
		'SELECT id_prof FROM profs WHERE prenom = "'+data.prof_prenom+'"',
		'SELECT id_prof FROM profs WHERE nom = "'+data.prof_nom+'"',
		'SELECT id_salle FROM salles WHERE salle = '+data.salle
	];
	tests_generaux(to_test.length-1, to_test, requetes, function(dv_array) {
		var dv = { duree: data.duree, jour: dv_array[0], h_debut: dv_array[1], mat_code: dv_array[2], classe: dv_array[3], prof_nom: dv_array[4], prof_prenom: dv_array[5], salle: dv_array[6] }
		callback(dv);
	});
}

function requete_ids(data, main_callback) {

	verification_donnees(data, function(dv) {
		var rsql = 'SELECT ';

		var duree = 1;
		if (dv.duree === '2h00' || dv.duree === '2h30') {
			duree = 2;
		}

		rsql += add_test([dv.jour, dv.h_debut], 'periode.id_periode, ');
		rsql += add_test(dv.mat_code, 'branches.id_branche, ');
		rsql += add_test(dv.classe, 'classes.id_classe, ');
		rsql += add_test([dv.prof_nom, dv.prof_prenom], 'profs.id_prof, ');
		rsql += add_test(dv.salle, 'salles.id_salle, ');

		if (/^SELECT\ $/.test(rsql)) { return; }
		rsql = rsql.replace(/,\ $/, ' ');
		rsql += 'FROM periode, branches, classes, profs, salles WHERE 1 ';

		rsql += add_test([dv.jour, dv.h_debut], 'AND periode.id_jour IN (SELECT id_jour FROM jours WHERE jour = "'+dv.jour+'") AND id_heure IN (SELECT id_heure FROM heures WHERE heure_debut = "'+dv.h_debut+'") ');
		rsql += add_test(dv.mat_code, 'AND branches.code_branche = "'+dv.mat_code+'" ');
		rsql += add_test(dv.classe, 'AND classes.classe = "'+dv.classe+'" ');
		rsql += add_test([dv.prof_nom, dv.prof_prenom], 'AND profs.nom = "'+dv.prof_nom+'" AND profs.prenom = "'+dv.prof_prenom+'" ');
		rsql += add_test(dv.salle, 'AND salles.salle = 128 ');

		rsql = rsql.replace(/\ $/, ' LIMIT 1');

		main_callback(rsql);
	});

}

exports.insert_elements = function(data, callback) {
	for (var i = 0; i < data.length; i++) {
		requete_ids(data[i], function(requete) {
			// sql.requete(requete, function(results) {
			// 	var requete_insersion = 'INSERT INTO horaires(id_periode, duree, id_classe, id_branche, id_prof, id_salle) VALUES ('+results[0].id_periode+', '+1+', '+results[0].id_classe+', '+results[0].id_branche+', '+results[0].id_prof+', '+results[0].id_salle+')';
			// 	requete_insersion = requete_insersion.replace(/undefined/g, 'NULL');
			// 	console.log(requete_insersion);
			// });
			callback(requete);
		});
	}
}