var creation_horaire = require('./creation_horaire.js');

exports.f = function(io, mysql, sql) {

  io.on('connection', function (socket) {

    socket.on('requete_sql', function(requete_sql) {
      if (requete_sql === '') {
        socket.emit('reponse', '<span style="color:red;">La requête est vide</span>');
        return;
      }

      sql.requete(requete_sql, function(results) {
        socket.emit('reponse', JSON.stringify(results));
      });
    });

    socket.on('execution_programme', function(data) {
      creation_horaire.insert_elements(data, function(rsql) {
        socket.emit('reponse', rsql);
      });

    });

  });

}

