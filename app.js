var http = require('http');
var path = require('path');
var express = require('express');
// récupération du module node-mysql
var mysql = require('mysql');

var app = express();
var server = http.Server(app);
var io = require('socket.io')(server);

var routes = require('./serveur/routes.js');
var socket = require('./serveur/socket.js');
// récupération des fonctions du fichier sql.js
var sql = require('./serveur/sql.js');

routes.f(app, __dirname, mysql, sql);
socket.f(io, mysql, sql);

server.listen(8888);