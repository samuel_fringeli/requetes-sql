socket.on('reponse', function(reponse) {
	byId('resultat').innerHTML = reponse;
});

function executer_requete() {
	socket.emit('requete_sql', byId('sql').value);
}

function executer_programme() {
	var data = byId('json').value;
	var formatted = JSON.parse(data);
	socket.emit('execution_programme', formatted);
}

function import_donnees(id) {
	byId(id).value = byId('resultat').innerHTML;
}

function conversion_csv() {
	csv = byId('csv').value.replace(/\n/g, '|');
	byId('resultat').innerHTML = csv;
	
	var lines = csv.split('|');
	var all = new Array(lines.length);

	for (var i = 0; i < lines.length; i++) {
		all[i] = lines[i].split(';');
	}

	var datas = new Array(all.length);
	for (var i = 0; i < all.length; i++) {
		datas[i] = {
			duree: all[i][1],
			jour: all[i][13],
			h_debut: all[i][14],
			mat_code: all[i][3],
			classe: all[i][7],
			prof_nom: all[i][5],
			prof_prenom: all[i][6],
			salle: all[i][8]
		}
	}
	
	byId('resultat').innerHTML = JSON.stringify(datas);
}